import pygame
import random

# Initialize Pygame
pygame.init()

# Set up colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 128, 0)

# Set up fonts
font = pygame.font.Font(None, 36)

# Set up screen dimensions
SCREEN_WIDTH = 1400
SCREEN_HEIGHT = 800

# Set up card dimensions
CARD_WIDTH = 100
CARD_HEIGHT = 150

# Define card values
card_values = {'2♦': 2, '3♦': 3, '4♦': 4, '5♦': 5, '6♦': 6, '7♦': 7, '8♦': 8, '9♦': 9, '10♦': 10, 'J♦': 11, 'Q♦': 12, 'K♦': 13, 'A♦': 14,
               '2♠': 2, '3♠': 3, '4♠': 4, '5♠': 5, '6♠': 6, '7♠': 7, '8♠': 8, '9♠': 9, '10♠': 10, 'J♠': 11, 'Q♠': 12, 'K♠': 13, 'A♠': 14,
               '2♣': 2, '3♣': 3, '4♣': 4, '5♣': 5, '6♣': 6, '7♣': 7, '8♣': 8, '9♣': 9, '10♣': 10, 'J♣': 11, 'Q♣': 12, 'K♣': 13, 'A♣': 14}

# Function to initialize the diamond deck
def initialize_diamond_deck():
    diamond_deck = [card for card in card_values.keys() if '♦' in card]
    random.shuffle(diamond_deck)
    return diamond_deck

# Function to display text on the screen
def draw_text(text, font, color, x, y):
    text_surface = font.render(text, True, color)
    text_rect = text_surface.get_rect()
    text_rect.topleft = (x, y)
    screen.blit(text_surface, text_rect)

# Function to run the game
def play_game():
    # Welcome message
    screen.fill(GREEN)
    draw_text("Welcome to Diamonds Game!", font, WHITE, 500, 300)
    pygame.display.flip()
    pygame.time.wait(1000)  # Display for 1 second

    screen.fill(GREEN)
    pygame.display.flip()
    pygame.time.wait(500)

    # Initialize the diamond deck
    diamond_deck = initialize_diamond_deck()

    # Banker's bid
    banker_bid = diamond_deck[0]
    screen.blit(card_images[banker_bid], (SCREEN_WIDTH // 2 - CARD_WIDTH // 2, SCREEN_HEIGHT // 4))
    pygame.display.flip()
    pygame.time.wait(2000)  # Display for 2 seconds

    # Display player 1's cards (initially all spades)
    player1_cards = ['2♠', '3♠', '4♠', '5♠', '6♠', '7♠', '8♠', '9♠', '10♠', 'J♠', 'Q♠', 'K♠', 'A♠']
    for i, card in enumerate(player1_cards):
        screen.blit(card_images[card], (20 + i * (CARD_WIDTH + 5), SCREEN_HEIGHT - CARD_HEIGHT - 20))
    pygame.display.flip()
    pygame.time.wait(2000)  # Display for 2 seconds

    # Display player 2's cards (initially all clubs)
    player2_cards = ['2♣', '3♣', '4♣', '5♣', '6♣', '7♣', '8♣', '9♣', '10♣', 'J♣', 'Q♣', 'K♣', 'A♣']
    for i, card in enumerate(player2_cards):
        screen.blit(card_images[card], (20 + i * (CARD_WIDTH + 5), 20))
    pygame.display.flip()
    pygame.time.wait(2000)  # Display for 2 seconds

    # Initialize players' scores
    player1_score = 0
    player2_score = 0
    rounds = 0
    player1_bid = None
    player2_bid = None

    # Main game loop
    running = True
    while running:
        screen.fill(GREEN)

        # Display diamond card
        if diamond_deck:
            current_card = diamond_deck[0]
            screen.blit(card_images[current_card], (SCREEN_WIDTH // 2 - CARD_WIDTH // 2, SCREEN_HEIGHT // 4))

        # Display player 1's cards
        for i, card in enumerate(player1_cards):
            screen.blit(card_images[card], (20 + i * (CARD_WIDTH + 5), SCREEN_HEIGHT - CARD_HEIGHT - 20))

        # Display player 2's cards
        for i, card in enumerate(player2_cards):
            screen.blit(card_images[card], (20 + i * (CARD_WIDTH + 5), 20))

        # Display player 1's score
        draw_text("Player 1 Score: " + str(player1_score), font, BLACK, SCREEN_WIDTH - 300, SCREEN_HEIGHT - 220)

        # Display player 2's score
        draw_text("Player 2 Score: " + str(player2_score), font, BLACK, SCREEN_WIDTH - 300, 190)

        pygame.display.flip()

        # Event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                # Check if the player clicked on a card
                x, y = pygame.mouse.get_pos()
                for i, card in enumerate(player1_cards):
                    if 20 + i * (CARD_WIDTH + 5) <= x <= 20 + i * (CARD_WIDTH + 5) + CARD_WIDTH and SCREEN_HEIGHT - CARD_HEIGHT - 20 <= y <= SCREEN_HEIGHT - 20:
                        # Player 1 bid a card
                        player1_bid = player1_cards.pop(i)
                        break
                for i, card in enumerate(player2_cards):
                    if 20 + i * (CARD_WIDTH + 5) <= x <= 20 + i * (CARD_WIDTH + 5) + CARD_WIDTH and 20 <= y <= 20 + CARD_HEIGHT:
                        # Player 2 bid a card
                        player2_bid = player2_cards.pop(i)
                        break

        # Check if both players have bid
        if player1_bid is not None and player2_bid is not None:
            # Remove the diamond card from the deck
            diamond_deck.pop(0)
            # Compare bids and update scores accordingly
            if card_values.get(player1_bid, 0) == card_values.get(player2_bid, 0):
                split_value = card_values[current_card] // 2
                player1_score += split_value
                player2_score += split_value
            elif card_values.get(player1_bid, 0) > card_values.get(player2_bid, 0):
                player1_score += card_values[current_card]
            else:
                player2_score += card_values[current_card]
            # Reset bids
            player1_bid = None
            player2_bid = None
            rounds += 1

    # Determine the winner
    if player1_score > player2_score:
        winner_text = "Player 1 wins!"
    elif player2_score > player1_score:
        winner_text = "Player 2 wins!"
    else:
        winner_text = "It's a tie!"

    # Display the winner
    draw_text(winner_text, font, BLACK, SCREEN_WIDTH // 2 - 100, SCREEN_HEIGHT // 2)
    pygame.display.flip()
    # pygame.time.wait(2000)  # Display for 2 seconds

    pygame.quit()

# Set up the screen
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Diamonds Game")

# Load card images
# Assuming card_images is a dictionary mapping card names to Pygame Surface objects
# You need to load these images before running the game
card_images = {
    # Diamonds cards
    '2♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/2_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '3♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/3_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '4♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/4_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '5♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/5_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '6♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/6_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '7♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/7_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '8♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/8_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '9♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/9_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '10♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/10_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'J♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/jack_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'Q♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/queen_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'K♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/king_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'A♦': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/ace_of_diamonds.png'), (CARD_WIDTH, CARD_HEIGHT)),

    # Spades cards
    '2♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/2_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '3♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/3_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '4♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/4_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '5♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/5_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '6♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/6_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '7♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/7_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '8♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/8_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '9♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/9_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '10♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/10_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'J♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/jack_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'Q♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/queen_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'K♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/king_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'A♠': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/ace_of_spades.png'), (CARD_WIDTH, CARD_HEIGHT)),

    # Clubs cards
    '2♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/2_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '3♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/3_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '4♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/4_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '5♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/5_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '6♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/6_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '7♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/7_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '8♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/8_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '9♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/9_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    '10♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/10_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'J♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/jack_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'Q♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/queen_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'K♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/king_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
    'A♣': pygame.transform.scale(pygame.image.load('PNG-cards-1.3/ace_of_hearts.png'), (CARD_WIDTH, CARD_HEIGHT)),
}

# Start the game
play_game()